AJAX

1. Create todo-list application with simple interface. Application should look like page with 4 areas. 
Each area should load data via AJAX from 'data[N].json' file (where N - number from 1 to 4).
Data for 1st area should load with delay in 1 sec, ...,  for 4th area - in 4 sec.